# Python 笔记



**离线下载安装包**
下载单个离线包 - pip download -d your_offline_packages <package_name>
批量下载离线包 - pip download -d your_offline_packages -r requirements.txt

**离线安装**
安装单个离线包 - pip install --no-index --find-links=/your_offline_packages/ package_name
批量安装离线包 - pip install --no-index --find-links=/your_offline_packages/ -r requirements.txt

