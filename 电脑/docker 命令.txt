获取镜像
docker pull <仓库名>:<标签>

运行镜像，启动容器
docker run --name <容器名> -d -p 80:80 <仓库名>:<标签>
docker run -it --rm <仓库名>:<标签> bash

列出镜像
docker images
docker images <仓库名>

保存新镜像
docker commit <容器ID或容器名> [<仓库名>[:<标签>]]
--author  --message

删除镜像 remove image
docker rmi <镜像>
docker rmi <镜像1> [<镜像2> ...]

启动容器（新建并启动）
docker run -d <仓库名>:<标签>
docker run --name <容器名> -d -p 80:80 <仓库名>:<标签>

启动已终止容器
docker start <容器ID或容器名>

查看容器
docker ps
docker ps -a

终止容器
docker stop <容器ID或容器名>

重启容器
docker restart <容器ID或容器名>

进入容器
docker exec -it <容器名> bash

查看容器改动
docker diff <容器ID或容器名>

导出容器
sudo docker export <容器ID或容器名> > name.tar

导入容器
cat name.tar | sudo docker import - <容器名>:v1.0

删除容器
docker rm <容器ID或容器名>

查看容器信息
docker inspect <容器ID或容器名>

清理所有处于终止状态的容器
docker rm $(docker ps -a -q)

Dockerfile
FROM <仓库名>
RUN
COPY <源路径> <目标路径>
CMD <命令>
CMD ["可执行文件", "参数1", "参数2"...]

按 Dockerfile 构建镜像
docker build -t nginx:v3 .
docker build -f ./Dockerfile -t nginx:v3 ./dist

数据卷（Volume 用法）

创建数据容器卷
docker volume create <容器卷名称>

查看所有容器卷
docker volume ls

查看指定容器卷详情信息
docker volume inspect <容器卷名称>

运行镜像使用数据容器卷
docker run -d -it --name=edc-nginx -p 8800:80 -v edc-nginx-vol:/usr/share/nginx/html nginx

移除数据卷
docker stop edc-nginx // 暂停容器实例
docker rm edc-nginx // 移除容器实例
docker volume rm edc-nginx-vol // 删除自定义数据卷

数据卷（Bind Mounts 用法）

创建数据容器卷
docker run -d -it -p 8800:80 --name=edc-nginx -v /app/wwwroot:/usr/share/nginx/html nginx


