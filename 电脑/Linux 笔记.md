# Linux 笔记 #



## 常用软件

| 常用 GUI 软件 | 说明                  | 常用程序包      | 说明                           |
| ------------- | --------------------- | --------------- | ------------------------------ |
| GParted       | 分区管理器            | nano            | 文本编辑器                     |
| Wine          | Windows 程序模拟器    | python-dev      | python                         |
| Audacious     | 音频播放器            | python-pip      | python 包管理工具              |
| VLC           | 多媒体播放器          | cups-pdf        | PDF 打印机                     |
| LibreOffice   | 办公软件              | traceroute      | 路由追踪工具                   |
| Firefox       | 网页浏览器            | gnome-commander | GNOME 桌面快速强大的文件管理器 |
| Chromium      | 网页浏览器            | gnome-nettool   | GNOME 桌面网络工具             |
| OpenJDK       | Java 虚拟机           | kde-desktop     | KDE 桌面环境                   |
| Wireshark     | 网络嗅探器            | kde-full        | KDE 完整软件库                 |
| Piano Booster | 演奏MIDI文件          | xfce4           | XFCE 桌面程序                  |
| MusE          | MIDI 编辑器           |                 |                                |
| MuseScore     | 乐谱编辑器（支持ove） |                 |                                |



## 文件目录操作 ##

**pwd 输出当前目录路径**

pwd

**list 列出目录内容**  

ls [OPTION] [path]

-l  list 列出详细信息
-h human readable 文件尺寸按照人性化可读性显示，如1K，234M，2G
-a  all 列出所有文件，包括以 . 开头的

第一列是这个文件的属性，一共11位字符
第一位表示这个文件的性质，d 代表目录，- 代表文件，l 代表链接文件（link file），b 代表设备文件中可供存储的接口设备，c 代表设备文件中的串行端口设备，例如键盘、鼠标。
第二位开始，每3位为一组，分别表示 user，group，other 的权限（r 代表 read, w 代表 write, x 代表 execute）。
第二列表示连接占用的节点（i-node）
第三列表示这个文件或目录的“拥有者”
第四列表示拥有者的用户组
第五列为这个文件的大小
第六列为这个文件的创建日期或者是最近的修改日期
第七列为这个文件的文件名：如果文件名前面有.说明是隐藏文件。

**mv 移动/重命名文件**  
mv [OPTION] &lt;SOURCE&gt; &lt;DEST&gt;  

-f force 不需要询问强制覆盖写入

**cp 拷贝文件**
cp [OPTION] &lt;SOURCE&gt; &lt;DEST&gt;  

-f force 不需要询问强制覆盖写入  
-R recursive 递归拷贝，包括所有子目录（默认忽略子目录）  
-v verbose 显示详细信息

**more 查看文件内容**

more <FILE&gt;

逐行查看，按回车键转到下一行，按 q 退出。

**nano 编辑文件内容**

nano [OPTIONS] FILE

需要额外安装

进入主界面后  
Ctrl+X  Exit 退出  
Ctrl+O  保存文件  
Ctrl+Y  上一页  
Ctrl+V  下一页  
Ctrl+K  Cut 删除一行  
Ctrl+W  搜索

**echo "" > FILE**

写入空白内容到一个文件，可以用于清空某个文件。

**find 查找文件**

find / -name pcre  
在 / 目录查找文件名包含 pcre 的结果

## 更改时区

sudo rm -f /etc/localtime
sudo ln -s /usr/share/zoneinfo/Asia/Shanghai /etc/localtime

**tzselect 查看时区的值**

## 系统运行情况 ##

**查看系统资源整体使用情况**
top
顶部包含了进程信息、处理器信息、内存使用信息
%Cpu(s)  代表 CPU 每秒的百分比
us  用户空间占用CPU百分比
sy  内核空间占用CPU百分比
ni  用户进程空间内改变过优先级的进程占用CPU百分比
id  空闲CPU百分比
wa  等待输入输出的CPU时间百分比
hi  硬中断（Hardware IRQ）占用CPU的百分比
si  软中断（Software Interrupts）占用CPU的百分比
**Mib Mem  内存信息**
total  物理内存总量
used   已使用的物理内存总量
free   空闲内存总量
buff/cache  用作内核缓存的内存量
KiB Swap  交换区
total  交换区总量
used   已使用的交换区总量
free   空闲交换区总量
avail Mem  可用于进程下一次分配的物理内存数量
中间是进程信息
按 q 退出

**查看内存使用情况**
free -h
-h  human-readable，易读的，以 M 或 G 为单位显示

**查看硬盘使用情况**
df -h
-h  human-readable，易读的，以 M 或 G 为单位显示

**查看当前目录各子节点硬盘占用情况**
du -lh --max-depth=1

## 查看系统信息

**查看主机名**
/bin/hostname

**查看系统产品名称，可以判断当前机子是物理机还是虚拟机**
dmidecode -s system-product-name
有的系统不支持这个命令

lshw -class system

输出为网页格式
lshw -html >info.html

**查看 CPU 处理器信息**
lscpu

**检测虚拟化信息，可以判断当前机子是物理机还是虚拟机**
systemd-detect-virt

**查看系统启动模式**
systemctl get-default
返回值：
graphical.target 代表图形界面
multi-user.target 代表命令行界面

**设置系统默认启动模式**
systemctl set-default graphical.target
systemctl set-default multi-user.target

**查看系统参数**
/sbin/sysctl -a
/sbin/sysctl -a | grep <param-name>
-a -all  all variables

**编辑系统参数**
/etc/sysctl.conf

**重新加载系统参数**
/sbin/sysctl -p

**资源限制配置**
/etc/security/limits.conf

## 输出内容到文件 ##

**覆盖原有内容**
内容 > 文件
ls -al > ./files.tree

**追加内容**
内容 >> 文件
ls -al > ./files.tree

还可以对一些正在被其他进程使用的日志文件，进行清空
echo '' > /path/to/xxx.log

## 管道符号 ##

<命令1> | <命令2> ...

命令1执行的输出结果作为命令2的参数

输出内容超过一页可以用 more
service --help | more

## 以管理员身份运行 ##

**以管理员身份运行**
sudo <命令>

sudoer 配置文件，指定允许以管理员身份运行的用户。
/etc/sudoers

root	ALL=(ALL)	ALL
wen		ALL=(ALL)	ALL

## su 切换用户

**只切换用户，不改变环境变量**
su <user>

**切换用户，并更新用户的环境变量 (推荐这种方式)**
su - <user>

## vi 文本编辑器

命令模式：

i  进入编辑模式，Esc 退出编辑模式
^  游标去到该行第一个字符
$  游标去到该行最后一个字符
Ctrl+F  下一页
Ctrl+B  上一页
dd  删除整行
(vim) u  撤销
(vim) Ctrl+R  恢复撤销
:q!  退出但不保存
:wq  写入并退出
/字符串  在游标后面查找字符串
?字符串  在游标前面查找字符串
:set nu  显示行号
:set nonu  不显示行号
:set fileencoding  查看文档编码



## 用户管理 ##

**用户信息记录文件**
/etc/passwd
格式
<UserName>:x:<UID>:<GID>:<Note>:<HomeFolder>:<Shell>

**用户密码记录文件**
/etc/shadow

**修改密码**
passwd <username>

**查看所有用户命令**
groups

**切换用户**
su <user>

**创建用户**
adduser <username>
该命令会在 /etc/passwd 添加一行记录，并创建用户目录
-d home_dir 设定使用者的家目录为 home_dir ，预设值为预设的 home 后面加上使用者帐号 loginid

**删除用户**
userdel <loginid>

## rpm  RedHat / CentOS 软件包管理器

**查询(query)某个包是否已安装**
rpm -q <package_name>

## yum RedHat / CentOS 软件包下载器 ##

**搜索软件包**
yum search <name>

**查看软件包信息**  
yum info <name>

**安装软件包**
yum install <name>
-y  一键确认，中途不再询问

**卸载软件包**

yum remove <name>

**更新已安装的软件**  
yum update

## apt Ubuntu 使用 DEB 软件包管理器 ##

**安装 apt 软件包**
apt-get install <name>

## 网络功能 ##

**查看网卡(interface)设置**  
ifconfig <devicename>

lo 是本地网络

网卡配置文件在：/etc/sysconfig/network-scripts/

配置文件参数
DEVICE   网卡名称
ONBOOT=yes  系统启动时启用
IPADDR  IP address IP 地址
NETMASK  网络掩码
GATEWAY  默认网关

**禁用网卡**
ifdown <devicename>

**启用网卡**
ifup <devicename>

**ping (测试外部主机的连通性)**  
ping HOST
HOST 可以是域名或 IP  
按 Ctrl+C 停止

**发送一个 HTTP 请求并打印内容**
curl <url>

**发送一个 POST 请求**
curl -X POST --header "Content-Type:multipart/form-data" --data "" <url>
curl -X POST --header "Content-Type:application/json" --data "{}" <url>

**下载文件**
wget <url>

**排查网卡硬件故障**
ethtool [option...] devname

需要额外安装

官方文档：
https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/6/html/deployment_guide/s1-ethtool

**查看路由状况**
route -n

**解析域名**  
nslookup HOST [DNS_HOST]  
如果不指定 DNS_HOST，则使用当前系统所使用的 DNS 服务器

dig HOST

**路由追踪**

traceroute <ip/domainName>

需要安装软件包

**检测端口能否正常连接**

telnet <ip/domainName>

## 服务管理 ##

**启动服务**
service <serviceName> start

**重启服务**
service <serviceName> restart

**停止服务**
service <serviceName> stop

**查看服务状态**
service <serviceName> status

**设置服务开机启动**
systemctl enable <serviceName>

## firewalld 防火墙 ##

CentOS 7 以上版本自带的防火墙

**查看防火墙状态**
firewall-cmd --state

**更新防火墙规则**
firewall-cmd --reload

**查看所有允许的服务**
firewall-cmd --list-services

**查看所有打开的端口**
firewall-cmd --list-ports

**获取所有支持的服务**
firewall-cmd --get-services

**永久添加允许的服务**
firewall-cmd --add-service ssh --permanent
firewall-cmd --add-service http --permanent
firewall-cmd --add-service https --permanent

**永久添加允许的端口**
firewall-cmd --add-port=<port>/tcp --permanent

## iptables 防火墙 ##

CentOS 6 以下版本自带的防火墙

**启动服务**
/etc/init.d/iptables start

**保存规则到配置文件**
service iptables save

## UFW 防火墙  ##

Ubuntu 自带的防火墙

**启用防火墙**
ufw enable

**禁用防火墙**
ufw disable

**允许 ssh 端口通过(支持端口名称和端口号)**
ufw allow ssh
ufw allow 21

**允许 ftp 端口通过(支持端口名称和端口号)**
ufw allow ftp
ufw allow 25

**允许 www 端口通过(支持端口名称和端口号)**
ufw allow www
ufw allow 80

## SE-Linux ##

配置文件：/etc/selinux/config

**获取当前模式**
getenforce

**设置规则**
setsebool

-P  代表永久修改

**设置 FTP**
setsebool -P allow_ftpd_full_access=1
setsebool -P ftp_home_dir=1

## 压缩包操作 ##

**创建一个 tar 压缩包文件**
tar zcvf test1.tar.gz /home/www

**解压 tar 压缩包文件**
tar zxvf test1.tar.gz

**查看 tar 压缩包的文件**
tar -tvf /home/bk0406-1717.tar.gz

## 发送邮件 ##

**安装 mailx**
yum install -y mailx

编辑配置文件 /etc/mail.rc，以 outlook 邮箱为例
set from="example@outlook.com"
set smtp="smtp.office365.com"
set smtp-auth-user="example@outlook.com"
set smtp-auth-password="password"
set smtp-auth=login
set smtp-use-starttls
set ssl-verify=ignore
set nss-config-dir=/etc/pki/nssdb/

**用 mailx 命令发送一个测试邮件**
<code> echo 'hello, world' | mailx -v -s "test" example@outlook.com </code>

**用 mail 命令发送一个测试邮件**
<code> echo 'hello, world' | mail -v -s "test" example@outlook.com </code>

## SSH 服务

安装：
CentOS: yum install -y sshd
Ubuntu: apt-get install ssh

配置：vi /etc/ssh/sshd_config
PermitRootLogin no			// 不允许 Root 用户登录
PermitEmptyPasswords no		// 不允许空密码登录
AllowUsers kevin			// 只允许指定用户登录

服务程序：/etc/init.d/ssh

**启动服务**
service sshd start

## FTP服务 ##

安装：
Ubuntu  #apt-get install vsftpd
CentOS  #yum install -y vsftpd

配置文件：
Ubuntu  /etc/vsftpd.conf
CentOS  /etc/vsftpd/*

服务程序：
/etc/init.d/vsftpd

根目录：
Ubuntu  /srv/ftp
CentOS  /var/ftp

默认禁止根目录的写权限，如果出错则清除根目录的写入权限
sudo chmod a-w ftp

匿名用户访问配置
anonymous_enable=YES  查找并去掉注释

匿名用户上传权限
anon_upload_enable=YES  查找并去掉注释

匿名用户创建目录的权限
anon_mkdir_write_enable=YES 查找并去掉注释

匿名用户权限掩码
anon_umask=066  手动添加(建议先查找一下)，默认为066

## Tomcat (Java Web Server) ##

**启动**  
bin/startup.sh

**关闭**  
bin/shutdown.sh  

**服务配置**  
conf/server.xml 文件

**应用内容配置**  
conf/context.xml 文件

**运行日志**  
logs/catalina.out 文件

## GoAgent 配置 ##

**下载 goagent**

**安装 gevent**
sudo apt-get install python-dev python-pip
sudo pip install gevent --upgrade

**运行 goagent**
$ cd local
$ python proxy.py

## MySQL Workbench ##

设置 mysql 命令路径

菜单”Edit”=>“Preferences...”，转到“Administrator”。

以 XAMPP 为例
Path to mysqldump Tool: /opt/lampp/bin/mysqldump
Path to mysql Tool: /opt/lampp/bin/mysql

## ACME 网站 SSL 免费证书生成 ##

**安装 ACME**
curl https://get.acme.sh | sh

**用 ACME 生成证书**
sudo ./acme.sh --issue -d breezes.name -d www.breezes.name -w /data/www/breezes/
-d 参数：域名
-w 参数：网站目录

## 常用操作 ##

**转换MP3文件的ID3编码** 
mid3iconv -e GBK *.mp3

**Word 转 Pdf**
soffice --headless --invisible --convert-to pdf <源office文档路径> --outdir <目录存储目录>

