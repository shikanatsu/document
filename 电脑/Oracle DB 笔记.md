# Oracle DB 笔记



## 安装配置



**oracle 用户添加 ORACLE_HOME 环境变量**

su - oracle
cd $HOME
./.bash_profile
结尾添加：
export ORACLE_HOME=/opt/oracle/product/19c/dbhome_1
export PATH=$PATH:/opt/oracle/product/19c/dbhome_1/bin
export ORACLE_SID=oracle19c

## SQL*Plus

命令不区分大小写

oracle 用户执行
sqlplus "/as sysdba"

sqlplus 程序里
startup 启动数据库实例
shutdown 关闭数据库实例
quit 或 exit 退出 SQL*Plus

select * from v$log;

## SQL

**查询所有表和注释**

```
SELECT a.TABLE_NAME, b.COMMENTS
FROM user_tables a, user_tab_comments b
WHERE a.TABLE_NAME=b.TABLE_NAME
ORDER BY TABLE_NAME 
```

**查询表的字段和注释**

```
SELECT c.COLUMN_NAME, c.DATA_TYPE, c.DATA_LENGTH, c.NULLABLE, c.DATA_DEFAULT, c2.COMMENTS
FROM USER_TAB_COLUMNS c, USER_COL_COMMENTS c2
WHERE c.TABLE_NAME = 'WBJH_GS_WBS_GSDJXX'
  AND c.TABLE_NAME = c2.TABLE_NAME
  AND c.COLUMN_NAME = c2.COLUMN_NAME
```

