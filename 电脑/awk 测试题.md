# awk 测试题



## 试题

(1) 显示 /etc/passwd 的账户

windows 下使用以下文件内容，保存到 passwd 文件
`root:x:0:0:root:/root:/bin/bash`
`bin:x:1:1:bin:/bin:/sbin/nologin`
`daemon:x:2:2:daemon:/sbin:/sbin/nologin`
`adm:x:3:4:adm:/var/adm:/sbin/nologin`
`lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin`

(2) 显示 /etc/passwd 的账户数量

(3)  统计 apache / nginx 日志单IP访问请求数排名

或使用以下文件内容，保存到 access.log 文件
10.0.0.41 - - [03/Dec/2010:23:27:01 +0800] "HEAD /checkstatus.jsp HTTP/1.0" 200 -
10.0.0.43 - - [03/Dec/2010:23:27:01 +0800] "HEAD /checkstatus.jsp HTTP/1.0" 200 -
10.0.0.42 - - [03/Dec/2010:23:27:01 +0800] "HEAD /checkstatus.jsp HTTP/1.0" 200 -

(4) 统计目录里 jpg 图片的总大小

(5) 统计 apache / nginx 日志某一个 IP 的访问次数



## 参考答案：

(1) awk -F : '{ print $1 }' /etc/passwd
按冒号分隔，输出第1列

(2) awk 'END {print "count:", NR}' /etc/passwd

(3) awk '{ print $1 }' ./access.log | sort | uniq -c | sort -rn -k1
输出第1列数据，即IP列表，对IP进行排序，合并重复数据，多出一列总数，再进行一次总数的倒序排序

(4) 以下任一都可
ls -al *.jpg | awk 'BEGIN { sum=0 } { sum+=$5 } END { print sum }'
ls -al *.jpg | awk 'BEGIN { sum=0 } { sum+=$5 } END { print sum/1024/1024, "M" }'
ls -al *.jpg | awk '{sum+=$5} END {print sum}'

(5) awk '/127.0.0.1/ {count++;} END { print "127.0.0.1 IP count=", count }' ./access.log

