# npm 笔记 #



## npm 命令 ##

npm init

初始化项目，创建一个 package.json 文件

npm install  根据 package.json 安装依赖
npm install <packageName> 安装指定的包

-S, -save  添加项目依赖，把模块和版本号添加到 package.json 的 dependencies 部分
-D, -save-dev  添加项目开发依赖，把模块和版本号添加到 package.json 的 devdependencies 部分

devdependencies  一般配置测试框架，编译时不需要用到的

## npm 常用第三方包 ##

mysql		MySQL server and client library
ws			Web Socket server and client library
sqlite3		SQLite 3 library
vue			Vue library
axios       web 客户端网络请求库
font-awesome  Web 前端图标库
moment        Web 前端时间库


